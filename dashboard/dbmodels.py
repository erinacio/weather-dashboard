from werkzeug.security import generate_password_hash as gen_hash, check_password_hash as check_hash
from flask_login import UserMixin, AnonymousUserMixin

from . import consts as c
from . import db
from . import login_manager


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    uid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    uname = db.Column(db.Unicode(c.NAME_MAX_LENGTH), nullable=False, unique=True)
    password_hash = db.Column(db.String(100), nullable=False)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)


    def get_id(self):
        return self.uid

    @property
    def password(self):
        raise AttributeError('password is not writable')

    @password.setter
    def password(self, pwd):
        self.password_hash = gen_hash(pwd, 'pbkdf2:sha256', 16)

    def check_password(self, pwd):
        return check_hash(self.password_hash, pwd)

    @property
    def banned(self):
        return False

    @property
    def op(self):
        return True


class Anonymous(AnonymousUserMixin):
    @property
    def uid(self):
        return None

    @property
    def banned(self):
        return True

    @property
    def op(self):
        return False


login_manager.anonymous_user = Anonymous


class Page(db.Model):
    __tablename__ = 'pages'
    pid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.Unicode(c.TITLE_MAX_LENGTH), nullable=False, default='')
    content = db.Column(db.UnicodeText, nullable=False, default='')
    date = db.Column(db.DateTime, nullable=False)
    hidden = db.Column(db.Boolean, nullable=False, default=False)

    def __str__(self):
        return '{}\t{}'.format(self.pid, self.removed)
