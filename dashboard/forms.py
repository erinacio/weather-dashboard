from flask_wtf import FlaskForm as Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired as Required, Length, EqualTo, Regexp
from . import consts as c


class LoginForm(Form):
    uname = StringField('用户名', validators=[
        Required(),
        Length(c.NAME_MIN_LENGTH, c.NAME_MAX_LENGTH)
    ])
    password = PasswordField('密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH)
    ])
    submit = SubmitField('登录')


class ChangePasswordForm(Form):
    old_password = PasswordField('旧密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH)
    ])
    password = PasswordField('新密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH),
        EqualTo('password2')
    ])
    password2 = PasswordField('确认密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH)
    ])
    submit = SubmitField('修改密码')


class ChangePasswordForm(Form):
    old_password = PasswordField('旧密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH)
    ])
    password = PasswordField('新密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH),
        EqualTo('password2')
    ])
    password2 = PasswordField('确认密码', validators=[
        Required(),
        Length(c.PASSWORD_MIN_LENGTH)
    ])
    submit = SubmitField('修改密码')


class ChangeNameForm(Form):
    uname = StringField('用户名', validators=[
        Required(),
        Length(c.NAME_MIN_LENGTH, c.NAME_MAX_LENGTH),
        Regexp('^\w+$', 0, '用户名只能包含文字、数字、下划线')
    ])
    submit = SubmitField('修改昵称')


class EditPageForm(Form):
    title = StringField('标题', validators=[
        Required(),
        Length(max=c.TITLE_MAX_LENGTH)
    ])
    content = TextAreaField('正文', validators=[
        Required()
    ], render_kw={'class': 'form-control', 'rows': 50})
    submit = SubmitField('提交')
