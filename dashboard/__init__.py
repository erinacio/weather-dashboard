import os
from flask import Flask, render_template, flash, redirect, url_for, abort, jsonify
from flask import escape as flask_escape
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, current_user, login_required
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


class Config:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data.db')
    SQLALCHEMY_ECHO = (os.environ.get('FLASK_DEBUG') == '1')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get('SECRET_KEY')


app = Flask(__name__)
app.config.from_object(Config)
login_manager = LoginManager(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap(app)

login_manager.session_protection = 'strong'
login_manager.login_view = 'login'
login_manager.login_message = "未登录，需要登录"

from . import models as m
from . import forms as f
from . import utils as u
from . import consts as c


@app.template_filter('to_html')
def to_html(plaintext):
    lines = [line.strip() for line in plaintext.split('\n')]
    lines = ['<p>{}</p>'.format(flask_escape(line)) for line in lines if len(line) > 0]
    return ''.join(lines)


@app.route('/')
@login_required
def main():
    return render_template('main.html', pages=m.list_pages())


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = f.LoginForm()
    if form.validate_on_submit():
        try:
            m.login_user(form.uname.data, form.password.data, False)
            return redirect(url_for('main'))
        except RuntimeError as e:
            flash(e.args[0])
    return render_template("login.html", form=form,)


@app.route('/logout')
def logout():
    m.logout_user()
    flash('已注销')
    return redirect(url_for('main'))


@app.route('/user/<int:uid>', methods=['GET', 'POST'])
def user(uid):
    user = m.get_user_or_404(uid)
    form_uname = f.ChangeNameForm()
    form_pwd = f.ChangePasswordForm()
    if form_uname.validate_on_submit():
        m.change_uname(form_uname.uname.data)
        flash('修改昵称成功')
    if form_pwd.validate_on_submit():
        try:
            m.change_password(form_pwd.old_password.data, form_pwd.password.data)
            flash('修改密码成功')
        except RuntimeError as e:
            flash(e.args[0])
    return render_template("user.html", user=user, form_uname=form_uname, form_pwd=form_pwd)


@app.route('/page/<int:pid>')
def page(pid):
    page = m.get_page_or_404(pid)
    if page.hidden and not current_user.op:
        abort(404)
    return render_template('page.html', page=page, pid=pid)


@app.route('/page/new', methods=['GET', 'POST'])
@login_required
def page_new():
    form_edit = f.EditPageForm()
    if form_edit.validate_on_submit():
        m.create_page(form_edit.title.data, form_edit.content.data)
        flash('创建成功')
        return redirect(url_for('main'))
    return render_template("page_edit.html", method_title='创建', form_edit=form_edit)


@app.route('/page/<int:pid>/edit', methods=['GET', 'POST'])
@login_required
def page_edit(pid):
    page = m.get_page_or_404(pid)
    form_edit = f.EditPageForm()
    if form_edit.validate_on_submit():
        m.edit_page(pid, form_edit.title.data, form_edit.content.data)
        flash('修改成功')
        return redirect(url_for('page', pid=pid))
    form_edit.title.data = page.title
    form_edit.content.data = page.content
    return render_template("page_edit.html", method_title='编辑', form_edit=form_edit)


@app.route('/page/<int:pid>/set-hidden/<int:hidden>')
@login_required
def page_set_hidden(pid, hidden):
    # FIXME: CSRF
    if not current_user.op:
        abort(403)
    m.set_page_hidden_or_404(pid, bool(hidden))
    flash('设置成功')
    return redirect(url_for('main'))


@app.route('/api/fetch-all')
def api_fetch_all():
    pages = m.list_unhidden_pages()
    data = [{'title': page.title, 'content': page.content, 'date': page.date.strftime('%Y-%m-%d %H:%M')} for page in pages]
    return jsonify(data)

@app.cli.command()
def deploy():
    """Deploy application."""
    from getpass import getpass
    db.create_all()
    user = m.load_user(1)
    if user is None:
        pass1 = getpass('Password: ')
        pass2 = getpass('Retype: ')
        if pass1 != pass2:
            print('Password mismatch')
        elif len(pass1) < c.PASSWORD_MIN_LENGTH:
            print('Password too short')
        else:
            m._regist_user('admin', pass1)
