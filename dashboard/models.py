from werkzeug.exceptions import Forbidden
from flask_login import current_user, login_user as flask_login_user, logout_user
from datetime import datetime

from . import db, login_manager
from .dbmodels import User, Page


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


def regist_user(uname, password):
    if not current_user.op:
        raise Forbidden
    return _regist_user(uname, password)


def _regist_user(uname, password):
    user = User(uname=uname)
    user.password = password
    db.session.add(user)
    db.session.commit()


def login_user(uname, password, keep_login):
    user = User.query.filter_by(uname=uname).first()
    if user is None or not user.check_password(password):
        raise RuntimeError('密码错误')
    flask_login_user(user, keep_login)


def change_password(old, new):
    assert current_user.is_authenticated
    if not current_user.check_password(old):
        raise RuntimeError('旧密码错误')
    current_user.password = new
    db.session.commit()


def change_uname(uname):
    assert current_user.is_authenticated
    current_user.uname = uname
    db.session.commit()


def get_user_or_404(uid):
    return User.query.get_or_404(uid)


def get_user_by_uname(uname):
    return User.query.filter_by(uname=uname).first()


def list_pages():
    return Page.query.order_by(db.desc(Page.pid)).all()


def get_page_or_404(pid):
    return Page.query.get_or_404(pid)


def create_page(title, content):
    page = Page(title=title, content=content, date=datetime.now())
    db.session.add(page)
    db.session.commit()


def edit_page(pid, title, content):
    page = Page.query.get(pid)
    page.title = title
    page.content = content
    page.date = datetime.now()
    db.session.commit()


def set_page_hidden_or_404(pid, hidden):
    page = Page.query.get_or_404(pid)
    page.hidden = hidden
    db.session.commit()


def list_unhidden_pages():
    return Page.query.filter_by(hidden=False).order_by(db.desc(Page.pid)).all()