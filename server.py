from gevent.wsgi import WSGIServer
import sys, os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from dashboard import app

http_server = WSGIServer(('', 8080), app)
http_server.serve_forever()
