FROM python:3.6-alpine3.7
RUN apk update && apk add gcc musl-dev
COPY dashboard /app/dashboard/
COPY requirements.txt server.py /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN apk del gcc musl-dev
EXPOSE 8080
CMD ["/usr/local/bin/python3", "server.py"]
